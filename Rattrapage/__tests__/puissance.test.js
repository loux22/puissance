const puissance = require('../index.js')

describe("puissance", () => {
    test('true', () => {
        let power = new puissance(200, 5)
        expect(power.result).toBe(Math.pow(200, 5));

        let powerBase0 = new puissance(0, 1)
        expect(powerBase0.result).toBe(Math.pow(0, 1));

        let powerPuissance0 = new puissance(1, 0)
        expect(powerPuissance0.result).toBe(Math.pow(1, 0));

        let powerPaire = new puissance(-10, 2)
        expect(powerPaire.result).toBe(Math.pow(-10, 2));

        let powerImpaire = new puissance(-10, 3)
        expect(powerImpaire.result).toBe(Math.pow(-10, 3));

        let powerExposantNegatif = new puissance(3, -1)
        expect(powerExposantNegatif.result).toBe(Math.pow(3, -1));

        let powerMultiplication = new puissance(4*3, 3)
        expect(powerMultiplication.result).toBe(Math.pow(4*3, 3));

        let powerDivision = new puissance(3/4, 3)
        expect(powerDivision.result).toBe(Math.pow(3/4, 3));

        let powerExposantFloat = new puissance(3, 0.75)
        expect(powerExposantFloat.result).toBe(Math.pow(3, 0.75));

        let powerExposantFloatNegatif = new puissance(2, -0.5)
        expect(powerExposantFloatNegatif.result).toBe(Math.pow(2, -0.5));
    })

    test('exposant < 0', () => {
        let power = new puissance(2, -0.5)
        expect(power.exposant).toBeLessThan(0);
    })
})