module.exports = class Puissance {
    constructor(base, exposant) {
        this.exposant = exposant
        this.result = 1
        if (Number.isInteger(exposant)){
            if (exposant >= 0){
                for (let i = 0; i < exposant; i++) {
                    this.result *= base
                }
            } else if(exposant < 0){
                for (let i = 0; i > exposant; i--) {
                    this.result *= base
                }
                this.result = 1/this.result
            }
        } else{
            this.result = Math.exp(exposant * Math.log(base)) 
        }
        console.log(this.result)
        return this.result
    }
}




